# frozen_string_literal: true

RSpec.describe Linter do
  it 'has a version number' do
    expect(Linter::VERSION).not_to be nil
  end
end
