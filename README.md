# Linter

Inspired by [Gender Decoder](http://gender-decoder.katmatfield.com/) this Gem analyzed a given text for gender associated language.

Currently this Gem only works for English text but open to contributions for other languages.


## Installation

Add this line to your application's Gemfile:

```ruby
gem 'linter'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install linter

## Usage

```ruby
text = 'Collaborate closely with the manager. Analytics all the way.'
Linter::GenderAssociation.analyze(text)
# #<OpenStruct feminine_coded_word_counts={"collaborate" => 1}, masculine_coded_word_counts={"analytics" => 1}, trend="neutral">
text = 'He was working at the bar.'
Linter::PronounAssociation.analyze(text)
```

## CLI Usage -> currently broken

```console
linter example.md
#<OpenStruct feminine_coded_word_counts={}, masculine_coded_word_counts={"analytical"=>1}, trend="strongly masculine-coded">
#<OpenStruct feminine_coded_word_counts={}, masculine_coded_word_counts={"he"=>1}, trend="masculine-coded">
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`.

## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/lienvdsteen/linter. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Linter project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://gitlab.com/lienvdsteen/linter/blob/master/CODE_OF_CONDUCT.md).
