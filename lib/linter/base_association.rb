# frozen_string_literal: true

module Linter
  class BaseAssociation
    def self.analyze(text)
      result = OpenStruct.new(
        feminine_coded_word_counts: {},
        masculine_coded_word_counts: {},
        trend: ''
      )

      wordlists['feminine_coded'].each do |word|
        result.feminine_coded_word_counts.merge!(word_count(text, word))
      end

      wordlists['masculine_coded'].each do |word|
        result.masculine_coded_word_counts.merge!(word_count(text, word))
      end

      result.trend = calculate_trend(result)
      result
    end

    def self.word_count(text, word)
      if self::FULL_WORD
        regex = /\b#{word}\b/i
      else
        regex = /\b(#{word}\w*)\b/i
      end
      matches = text.scan(regex)
      return {} unless matches.any?

      # Use Enumerable#tally with Ruby 2.7
      matches
        .flatten
        .map(&:downcase)
        .group_by { |v| v }
        .transform_values(&:size)
        .to_h
    end
  end
end
